package model;

/**
 * Created by IntelliJ IDEA.
 * User: Marcus
 * Date: 3/5/2019
 */
public enum DayStatus {
	NONE, STARTDATE, ENDDATE;
}
