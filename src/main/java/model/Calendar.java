package model;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.Month;
import java.time.Year;
import java.util.LinkedHashMap;

/**
 * Created by IntelliJ IDEA.
 * User: Marcus
 * Date: 3/5/2019
 */
public class Calendar {

	private LinkedHashMap<LocalDate, DayStatus> calendar;
	private LocalDate start;
	private LocalDate end;

	private Calendar(Month month, int year) {
		Year y = Year.of(year);
		int days = month.length(y.isLeap());
		calendar = new LinkedHashMap<>();
		for(int i=1; i<=days;i++){
			LocalDate date = LocalDate.of(year,month,i);
			calendar.put(date,DayStatus.NONE);
		}
		start = LocalDate.of(year, month, 1);
		end = LocalDate.of(year, month, days);
	}

	public Calendar(LocalDate date){
		this(date.getMonth(),date.getYear());
	}

	public LinkedHashMap<LocalDate, DayStatus> getCalendar() {
		return calendar;
	}

	public void setStartDate(LocalDate date) throws Exception {
		if (!calendar.containsKey(date))
			throw new Exception("Date in not in this calendar");
		calendar.put(date, DayStatus.STARTDATE);
	}

	public void setEndDate(LocalDate date) throws Exception {
		if (!calendar.containsKey(date))
			throw new Exception("Date in not in this calendar");
		calendar.put(date, DayStatus.ENDDATE);
	}

	public void printCalendar(){
		System.out.print((String.format("|%17c", ' ')));
		System.out.println(String.format("%02d-%-21d|", start.getMonthValue(), start.getYear()));
		System.out.println("| Sun | Mon | Tue | Wed | Thu | Fri | Sat |");
		LocalDate next = start;
		while (!next.isAfter(end)) {
			next = printCalendarRow(next);
		}
	}

	private static LocalDate printCalendarRow(LocalDate start){
		Month month = start.getMonth();
		DayOfWeek starting = start.getDayOfWeek();
		int offset = 0;
		switch (starting) {
			case SATURDAY:
				offset++;
			case FRIDAY:
				offset++;
			case THURSDAY:
				offset++;
			case WEDNESDAY:
				offset++;
			case TUESDAY:
				offset++;
			case MONDAY:
				offset++;
			case SUNDAY:
				break;
		}
		StringBuilder line = new StringBuilder();
		for (int i = 0; i < 7; i++) {
			if (i < offset) {
				line.append("|     ");
			} else {
				line.append("|");
				line.append(String.format("%5d", start.getDayOfMonth()));
				start = start.plusDays(1);
				if (start.getMonth() != month) break;
			}
		}
		line.append("|");
		System.out.println(line.toString());
		return start;
	}



}
