import common.Contants;
import model.Calendar;
import util.DayUtil;
import util.HolidayUtil;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: Marcus
 * Date: 3/5/2019
 */
public class Main {
	public static void main(String ... s){
		String inputString;
		LocalDate inputDate = null;

		HolidayUtil holidayUtil = HolidayUtil.init();
		while(inputDate == null) {
			BufferedReader br;
			try {
				System.out.println("Enter the start date of the ticket (DD-MM-YYYY): ");
				br = new BufferedReader(new InputStreamReader(System.in));
				inputString = br.readLine();
				inputDate = LocalDate.parse(inputString, Contants.FORMATTER);
			} catch (Exception e) {
				inputDate = null;
				System.out.println("Error : Not a valid Date");
			}
		}
		System.out.println("Your Current Ticket Info");
		System.out.println("Start Date\t:\t" + inputDate);
		LocalDate endDate = DayUtil.calculateEndDate(inputDate); //the first also counted
		System.out.println("End Date\t:\t" + endDate);
		System.out.println("Covered WorkingDay\t:\t" + DayUtil.countWorkingDay(inputDate,endDate,true));

		//TODO implement the logic to print next start day (simple logic: next working day)

		//TODO implement the logic to print next start day (advanced logic: maximum working day covered)

		//TODO implement the print calendar
		List<Calendar> calendars = new LinkedList<>();
		LocalDate date = inputDate;
		for (int i = inputDate.getMonthValue(); i <= endDate.getMonthValue(); i++) {
			Calendar calendar = new Calendar(date);
			calendars.add(calendar);
			date = date.plusMonths(1);
		}

		for (Calendar calendar : calendars)
			calendar.printCalendar();


//		try {
//			calendar.setStartDate(inputDate);
//			calendar.setEndDate(endDate);
//		}catch(Exception e){
//
//		}

	}
}
