package util;

import java.time.DayOfWeek;
import java.time.LocalDate;

/**
 * Created by IntelliJ IDEA.
 * User: Marcus
 * Date: 3/5/2019
 */
public class DayUtil {

	public static int countWorkingDay(LocalDate start, LocalDate end, boolean countSat){
		int count = 0;
		LocalDate step = start;
		while(!step.isAfter(end)){
			if(isWorkingDay(step,countSat)){
				count++;
			}
			step = step.plusDays(1);
		}
		return count;
	}

	static boolean isWorkingDay(LocalDate date, boolean countSat) {
		if(HolidayUtil.getInstance().isHoliday(date)){
			return false;
		}
		if(date.getDayOfWeek()==DayOfWeek.SUNDAY){
			return false;
		}
		if(date.getDayOfWeek()==DayOfWeek.SATURDAY){
			return countSat;
		}
		return true;
	}

	public static LocalDate calculateEndDate(LocalDate start) {
		return start.plusDays(29);
	}

}
