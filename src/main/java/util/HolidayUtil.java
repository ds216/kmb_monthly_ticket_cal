package util;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import common.Contants;

import java.io.BufferedReader;
import java.io.IOException;
import java.lang.reflect.Type;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: Marcus
 * Date: 3/5/2019
 */
public class HolidayUtil {



	private static HolidayUtil instance = null;

	private List<LocalDate> holidays = null;

	private HolidayUtil(){
		Path resPath = null;
		try {
			//get resource file
			resPath = Paths.get(ClassLoader.getSystemResource("holiday.json").toURI());
		}catch(Exception e){
			e.printStackTrace();
		}
		if(resPath != null){
			if(Files.exists(resPath)){
				StringBuilder resContent = new StringBuilder();
				try(BufferedReader reader = Files.newBufferedReader(resPath, Charset.forName("UTF-8"))) {
					String currentLine = null;
					while((currentLine = reader.readLine()) != null){//while there is content on the current line
						resContent.append(currentLine);
						resContent.append("\n");
					}
					Gson gson = new Gson();
					Type collectionType = new TypeToken<Collection<String>>(){}.getType();
					List<String> listForDate = gson.fromJson(resContent.toString(), collectionType);
					holidays = new ArrayList<>();
					for(String date : listForDate){
						LocalDate localDate = LocalDate.parse(date, Contants.FORMATTER);
						holidays.add(localDate);
					}
				}catch (IOException e){
					e.printStackTrace();
				}
			}
		}
	}

	public boolean isHoliday(LocalDate date){
		if(holidays == null) return false;
		return holidays.contains(date);
	}

	public static HolidayUtil init(){
		if(instance == null){
			instance = new HolidayUtil();
		}
		return instance;
	}

	public static HolidayUtil getInstance(){
		return init();
	}

}
