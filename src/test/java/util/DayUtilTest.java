package util;

import common.Contants;
import org.junit.Assert;
import org.junit.Test;

import java.time.LocalDate;

import static org.junit.Assert.*;

/**
 * Created by IntelliJ IDEA.
 * User: Marcus
 * Date: 3/5/2019
 */
public class DayUtilTest {

	@Test
	public void countWorkingDayWithSat() {
		String start = "02-03-2019";
		String end = "09-03-2019";
		int result = DayUtil.countWorkingDay(LocalDate.parse(start, Contants.FORMATTER),LocalDate.parse(end, Contants.FORMATTER),true);
		Assert.assertEquals(result,7);
	}

	@Test
	public void countWorkingDayWithoutSat() {
		String start = "24-02-2019";
		String end = "04-04-2019";
		int result = DayUtil.countWorkingDay(LocalDate.parse(start, Contants.FORMATTER),LocalDate.parse(end, Contants.FORMATTER),false);
		Assert.assertEquals(result,29);
	}

	@Test
	public void countWorkingDayWithHoliday() {
		String start = "24-02-2019";
		String end = "06-04-2019";
		int result = DayUtil.countWorkingDay(LocalDate.parse(start, Contants.FORMATTER),LocalDate.parse(end, Contants.FORMATTER),false);
		Assert.assertEquals(result,29);
	}

	@Test
	public void isWorkingDayWithoutSat_Sun() {
		String start = "24-02-2019";
		boolean result = DayUtil.isWorkingDay(LocalDate.parse(start, Contants.FORMATTER),false);
		Assert.assertFalse(result);
	}

	@Test
	public void isWorkingDayWithoutSat_Mon() {
		String start = "04-03-2019";
		boolean result = DayUtil.isWorkingDay(LocalDate.parse(start, Contants.FORMATTER),false);
		Assert.assertTrue(result);
	}

	@Test
	public void isWorkingDayWithoutSat_Sat() {
		String start = "09-03-2019";
		boolean result = DayUtil.isWorkingDay(LocalDate.parse(start, Contants.FORMATTER),false);
		Assert.assertFalse(result);
	}

	@Test
	public void isWorkingDayWithSat_Sun() {
		String start = "24-02-2019";
		boolean result = DayUtil.isWorkingDay(LocalDate.parse(start, Contants.FORMATTER),true);
		Assert.assertFalse(result);
	}

	@Test
	public void isWorkingDayWithSat_Mon() {
		String start = "04-03-2019";
		boolean result = DayUtil.isWorkingDay(LocalDate.parse(start, Contants.FORMATTER),true);
		Assert.assertTrue(result);
	}

	@Test
	public void isWorkingDayWithSat_Sat() {
		String start = "09-03-2019";
		boolean result = DayUtil.isWorkingDay(LocalDate.parse(start, Contants.FORMATTER),true);
		Assert.assertTrue(result);
	}

	@Test
	public void isWorkingDayWithSat_Holiday() {
		String start = "22-04-2019";
		boolean result = DayUtil.isWorkingDay(LocalDate.parse(start, Contants.FORMATTER),true);
		Assert.assertFalse(result);
	}

	@Test
	public void isWorkingDayWithoutSat_Holiday() {
		String start = "22-04-2019";
		boolean result = DayUtil.isWorkingDay(LocalDate.parse(start, Contants.FORMATTER),true);
		Assert.assertFalse(result);
	}

	@Test
	public void isWorkingDayWithSat_HolidaySat() {
		String start = "20-04-2019";
		boolean result = DayUtil.isWorkingDay(LocalDate.parse(start, Contants.FORMATTER),true);
		Assert.assertFalse(result);
	}

	@Test
	public void isWorkingDayWithoutSat_HolidaySat() {
		String start = "20-04-2019";
		boolean result = DayUtil.isWorkingDay(LocalDate.parse(start, Contants.FORMATTER),true);
		Assert.assertFalse(result);
	}
}