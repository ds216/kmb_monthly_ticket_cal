package util;

import common.Contants;
import org.junit.Assert;
import org.junit.Test;

import java.time.LocalDate;

import static org.junit.Assert.*;

/**
 * Created by IntelliJ IDEA.
 * User: Marcus
 * Date: 3/5/2019
 */
public class HolidayUtilTest {

	@Test
	public void isHoliday() {
		String date = "25-12-2019";
		HolidayUtil.init();
		HolidayUtil util = HolidayUtil.getInstance();
		boolean result = util.isHoliday(LocalDate.parse(date, Contants.FORMATTER));
		Assert.assertTrue(result);
	}

	@Test
	public void isNotHoliday() {
		String date = "24-12-2019";
		HolidayUtil util = HolidayUtil.init();
		boolean result = util.isHoliday(LocalDate.parse(date, Contants.FORMATTER));
		Assert.assertFalse(result);
	}
}